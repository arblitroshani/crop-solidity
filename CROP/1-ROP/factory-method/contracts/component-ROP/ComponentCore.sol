// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./Component.sol";
import "./ComponentRole.sol";
import "./RoleCreator.sol";

contract ComponentCore is Component {

    mapping(bytes32 => ComponentRole) internal playedRoles;

    mapping(bytes32 => RoleCreator) internal roleCreators;

    /**
     * @dev Adds a `roleCreator` for a `spec`.
     *
     * If another `roleCreator` already exists for a `spec`, it gets overriden.
     */
    function addRoleCreator(bytes32 spec, RoleCreator roleCreator) external {
        roleCreators[spec] = roleCreator;
    }

    /**
     * @dev Grants a role that should be created from `spec`.
     * 
     * Returns the `address` of the newly created role instance.
     */
    function addRole(bytes32 spec) external override returns (address) {
        RoleCreator rc = roleCreators[spec];
        ComponentRole role = rc.createFor(spec);

        if (role != ComponentRole(address(0))) {
            playedRoles[spec] = role;
            role.setCore(this);
        }
        return address(role);
    }

    /**
     * @dev Revokes role with `spec` from `component`.
     *
     * If `component` has not been granted the role with `spec`, does nothing.
     */
    function removeRole(bytes32 spec) external override {
        if (!this.isPlayingRole(spec)) return; 
        ComponentRole componentRole = ComponentRole(playedRoles[spec]);
        componentRole.setCore(ComponentCore(address(0)));
        delete(playedRoles[spec]);
    }

    /**
     * @dev Returns `true` if `component` has been granted role with `spec`.
     */
    function isPlayingRole(bytes32 spec) external override view returns (bool) {
        return address(playedRoles[spec]) != address(0);
    }

    /**
     * @dev Returns the role address that is associated to `spec`. 
     *
     * If `component` has not been granted the role with `spec`, returns 0x00 address.
     */
    function getRole(bytes32 spec) external override returns (address) {
        ComponentRole role = playedRoles[spec];
        if (this.isPlayingRole(spec)) {
            role.setCore(this);
        }
        return address(role);
    }
}
