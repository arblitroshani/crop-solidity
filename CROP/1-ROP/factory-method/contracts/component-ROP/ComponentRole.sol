// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./Component.sol";
import "./ComponentCore.sol";

abstract contract ComponentRole is Component {

    // MARK: ComponentRole specific

    ComponentCore internal core;

    /**
     * @dev Assign the `ComponentCore` in .
     */
    function setCore(ComponentCore _core) external {
        core = _core;
    }

    /**
     * @dev Modifier that checks that `componentCore` has been set. 
     * Reverts with a descriptive message if the core has not been set.
     *
     * To be used for all role functions that require the core.
     */
    modifier onlyWhenActive {
        require(address(core) != address(0), "This function requires the core to be set.");
        _;
    }

    // MARK: Operations forwarded to the Component core object. 

    /**
     * @dev Grants a role that should be created from `spec`.
     * 
     * Returns the `address` of the newly created role instance.
     */
    function addRole(bytes32 spec) external override returns (address) {
        return core.addRole(spec);
    }

    /**
     * @dev Revokes role with `spec` from `component`.
     *
     * If `component` has not been granted the role with `spec`, does nothing.
     */
    function removeRole(bytes32 spec) external override {
        core.removeRole(spec);
    }  

    /**
     * @dev Returns `true` if `component` has been granted role with `spec`.
     */
    function isPlayingRole(bytes32 spec) external override view returns (bool) {
        return core.isPlayingRole(spec);
    }

    /**
     * @dev Returns the role address that is associated to `spec`. 
     *
     * If `component` has not been granted the role with `spec`, returns 0x00 address.
     */
    function getRole(bytes32 spec) external override returns (address) {
        return core.getRole(spec);
    } 
}
