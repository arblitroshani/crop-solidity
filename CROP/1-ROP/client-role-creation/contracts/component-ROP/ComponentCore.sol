// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./Component.sol";
import "./ComponentRole.sol";

contract ComponentCore is Component {

    mapping(bytes32 => ComponentRole) internal playedRoles;

    /**
     * @dev Grants `role` to `component` and associates it to `spec`.
     */
    function addRole(bytes32 spec, address role) external override {
        if (role == address(0)) return;
        ComponentRole componentRole = ComponentRole(role);
        playedRoles[spec] = componentRole;
        componentRole.setCore(this);
    }

    /**
     * @dev Revokes role with `spec` from `component`.
     *
     * If `component` has not been granted the role with `spec`, does nothing.
     */
    function removeRole(bytes32 spec) external override {
        if (!this.isPlayingRole(spec)) return; 
        ComponentRole componentRole = playedRoles[spec];
        componentRole.setCore(ComponentCore(address(0)));
        delete(playedRoles[spec]);
    }

    /**
     * @dev Returns `true` if `component` has been granted role with `spec`.
     */
    function isPlayingRole(bytes32 spec) external override view returns (bool) {
        return address(playedRoles[spec]) != address(0);
    }

    /**
     * @dev Returns the role address that is associated to `spec`. 
     *
     * If `component` has not been granted the role with `spec`, returns 0x00 address.
     */
    function getRole(bytes32 spec) external override returns (address) {
        ComponentRole role = playedRoles[spec];
        if (this.isPlayingRole(spec)) {
            role.setCore(this);
        }
        return address(role);
    }
}
