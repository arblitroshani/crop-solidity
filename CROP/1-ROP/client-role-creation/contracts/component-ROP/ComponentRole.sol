// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./Component.sol";
import "./ComponentCore.sol";

abstract contract ComponentRole is Component {

    ComponentCore internal core;

    /**
     * @dev Sets the `componentCore` to `role`.
     */
    function setCore(ComponentCore _core) external {
        core = _core;
    }

    /**
     * @dev Modifier that checks that `componentCore` has been set. 
     * Reverts with a descriptive message if the core has not been set.
     *
     * To be used for all role functions that require the core.
     */
    modifier onlyWhenActive {
        require(address(core) != address(0), "This function requires the core to be set.");
        _;
    }

    // MARK: Operations forwarded to the Component core object. 

    /**
     * @dev Grants `role` to `component` and associates it to `spec`.
     */
    function addRole(bytes32 _spec, address _role) external override {
        return core.addRole(_spec, _role);
    }

    /**
     * @dev Revokes role with `spec` from `component`.
     *
     * If `component` has not been granted the role with `spec`, does nothing.
     */
    function removeRole(bytes32 _spec) external override {
        core.removeRole(_spec);
    }  

    /**
     * @dev Returns `true` if `component` has been granted role with `spec`.
     */
    function isPlayingRole(bytes32 _spec) external override view returns (bool) {
        return core.isPlayingRole(_spec);
    }

    /**
     * @dev Returns the role address that is associated to `spec`. 
     *
     * If `component` has not been granted the role with `spec`, returns 0x00 address.
     */
    function getRole(bytes32 _spec) external override returns (address) {
        return core.getRole(_spec);
    } 
}
