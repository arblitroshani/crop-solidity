// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./Customer.sol";
import "./CustomerRole.sol";
import "./Team.sol";

library InterfaceIds {
    bytes4 constant CUSTOMER_ID = type(Customer).interfaceId;
    bytes4 constant CUSTOMER_ROLE_ID = type(CustomerRole).interfaceId;
    bytes4 constant TEAM_ID = type(Team).interfaceId;
}