// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./Customer.sol";
import "./CustomerCore.sol";
import "./Team.sol";
import "./InterfaceIds.sol";
import "../ERC165/ERC165Query.sol";

abstract contract CustomerRole is Customer, ERC165Query {

    CustomerCore internal core;

    Team internal assignedTeam;

    // MARK: ERC165

    /// @notice Query if a contract implements an interface
    /// @param interfaceID The interface identifier, as specified in ERC-165
    /// @dev Interface identification is specified in ERC-165. This function
    ///  uses less than 30,000 gas.
    /// @return `true` if the contract implements `interfaceID` and
    ///  `interfaceID` is not 0xffffffff, `false` otherwise
    function supportsInterface(bytes4 interfaceID) external override pure returns (bool) {
        return interfaceID == 0x01ffc9a7                     // ERC165
            || interfaceID == InterfaceIds.CUSTOMER_ROLE_ID; // CustomerRole
    }

    // MARK: CustomerRole specific

    /**
     * @dev Sets the `customerCore` to `role`.
     */
    function setCore(address coreAddress) external {
        if (coreAddress != address(0)) {
            bool isCustomer = doesContractImplementInterface(coreAddress, InterfaceIds.CUSTOMER_ID);
            require(isCustomer, "Doesn't support Customer interface.");
        }
        core = CustomerCore(coreAddress);
    }

    /**
     * @dev Sets the `team` to `role`.
     */
    function setTeam(address comptAddress) external {
        if (comptAddress != address(0)) {
            bool isCompt = doesContractImplementInterface(comptAddress, InterfaceIds.TEAM_ID);
            require(isCompt, "Doesn't support Team interface.");
        }
        assignedTeam = Team(comptAddress);
    }

    /**
     * @dev Modifier that checks that `customerCore` has been set and
     *  the core's `team` matches this role's `team`. 
     *
     * Reverts with a descriptive message if the core has not been set.
     *
     * To be used for all role functions that require the core.
     */
    modifier onlyWhenActive {
        require(
            address(core) != address(0), 
            "This function requires the core to be set.");
        require(
            core.getActiveTeam() == address(assignedTeam), 
            "The context for this role is not currently active.");
        _;
    }

    // MARK: Operations forwarded to the CustomerCore object. 

    /**
     * @dev Returns the auto generated `id` of the customer.
     */
    function getId() external view returns (uint) {
        return core.getId();
    }

    /**
     * @dev Grants `role` to `customer` and associates it to `spec`.
     */
    function addRole(bytes32 spec, address role) external {
        core.addRole(spec, role);
    }

    /**
     * @dev Revokes role with `spec` from `customer`.
     *
     * If `customer` has not been granted the role with `spec`, does nothing.
     */
    function removeRole(bytes32 spec) external override {
        core.removeRole(spec);
    }   

    /**
     * @dev Returns `true` if `customer` has been granted role with `spec`.
     */
    function isPlayingRole(bytes32 spec) external override view returns (bool) {
        return core.isPlayingRole(spec);
    }

    /**
     * @dev Returns the role address that is associated to `spec`. 
     *
     * If `customer` has not been granted the role with `spec`, returns 0x00 address.
     */
    function getRole(bytes32 spec) external override returns (address) {
        return core.getRole(spec);
    } 

    /**
     * @dev Sets a `team` to the `customer` to indicate entering a context.
     *
     * If another `team` has already been set, it gets overriden.
     */
    function activateTeam(address team) external override {
        core.activateTeam(team);
    }

    /**
     * @dev Unsets the `Team` if there is one activated.
     */
    function deactivateTeam() external override {
        core.deactivateTeam();
    }

    /**
     * @dev Returns the `address` of the activated `team`.
     */
    function getActiveTeam() external view override returns (address) {
        return core.getActiveTeam();
    }
}
