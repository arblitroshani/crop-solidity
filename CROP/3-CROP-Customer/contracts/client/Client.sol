// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "../customer-CROP/CustomerCore.sol";
import "../bank/Bank.sol";
import "../bank/Borrower.sol";

contract Client {

    bytes32 internal borrowerSpec = keccak256("BORROWER");

    CustomerCore internal customer = new CustomerCore();
    Bank internal bank = new Bank();

    function takeCarLoan() external {
        customer.activateTeam(address(bank));
        Borrower borrowerRole;

        if (customer.isPlayingRole(borrowerSpec)) {
            address borrowerRoleAddress = customer.getRole(borrowerSpec);
            borrowerRole = Borrower(borrowerRoleAddress);
        } else {
            borrowerRole = bank.getBorrowerRole();
            customer.addRole(borrowerSpec, address(borrowerRole));
        }

        borrowerRole.borrow(1000);
        customer.deactivateTeam();
    }
}
