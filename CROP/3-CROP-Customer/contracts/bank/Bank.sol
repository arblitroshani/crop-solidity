// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "../customer-CROP/CustomerRole.sol";
import "../customer-CROP/Team.sol";
import "./Borrower.sol";
import "./Investor.sol";
import "./BorrowerCreator.sol";
import "./InvestorCreator.sol";

contract Bank is Team {

    Borrower internal borrowerRole;
    Investor internal investorRole;

    bytes32 internal borrowerSpec = keccak256("BORROWER");
    bytes32 internal investorSpec = keccak256("INVESTOR");

    function getBorrowerRole() external returns (Borrower) {
        if (address(borrowerRole) != address(0)) return borrowerRole;

        this.addRoleCreator(borrowerSpec, new BorrowerCreator());
        borrowerRole = Borrower(this.addRole(borrowerSpec));
        return borrowerRole;
    }

    function getInvestorRole() external returns (Investor) {
        if (address(investorRole) != address(0)) return investorRole;

        this.addRoleCreator(investorSpec, new InvestorCreator());
        investorRole = Investor(this.addRole(investorSpec));
        return investorRole;
    }
}
