// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "../customer-CROP/CustomerRole.sol";

contract Investor is CustomerRole {

    uint internal investAmount;

    function invest(uint amount) external onlyWhenActive {
        investAmount += amount;
    }

    function withdraw(uint amount) external onlyWhenActive {
        if (amount > investAmount) investAmount = 0;
        else investAmount -= amount;
    }
}
