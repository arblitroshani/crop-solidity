// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "../customer-CROP/RoleCreator.sol";
import "./Investor.sol";

contract InvestorCreator is RoleCreator {

    function createFor(bytes32 spec) external returns (CustomerRole) {
        return new Investor();
    }
}
