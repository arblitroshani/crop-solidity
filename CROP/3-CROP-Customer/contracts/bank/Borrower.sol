// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "../customer-CROP/CustomerRole.sol";

contract Borrower is CustomerRole {

    uint internal borrowAmount;

    function borrow(uint amount) external onlyWhenActive {
        borrowAmount += amount;
    }

    function repay(uint amount) external onlyWhenActive {
        if (amount > borrowAmount) borrowAmount = 0;
        else borrowAmount -= amount;
    }

    function getBalance() external view onlyWhenActive returns (uint) {
        return borrowAmount;
    }
}
