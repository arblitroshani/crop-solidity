// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./Component.sol";
import "./ComponentRole.sol";
import "./Team.sol";

library InterfaceIds {
    bytes4 constant COMPONENT_ID = type(Component).interfaceId;
    bytes4 constant COMPONENT_ROLE_ID = type(ComponentRole).interfaceId;
    bytes4 constant TEAM_ID = type(Team).interfaceId;
}