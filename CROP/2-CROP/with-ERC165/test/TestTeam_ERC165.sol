// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.9.0;

import "truffle/Assert.sol";
import "../contracts/component-CROP/InterfaceIds.sol";
import "./fakes/FakeTeam.sol";

contract TestTeam_ERC165 {

    FakeTeam internal team;

    function beforeEach() public {
        team = new FakeTeam();
    }

    function testSupportsErc165() public {
        Assert.isTrue(team.supportsInterface(0x01ffc9a7), "");
    }

    function testSupportsTeamInterface() public {
        Assert.isTrue(team.supportsInterface(InterfaceIds.TEAM_ID), "");
    }

    function testDoesNotSupportComponentRoleInterfaceId() public {
        Assert.isFalse(team.supportsInterface(InterfaceIds.COMPONENT_ROLE_ID), "");
    }

    function testDoesNotSupportComponentInterfaceId() public {
        Assert.isFalse(team.supportsInterface(InterfaceIds.COMPONENT_ID), "");
    }

    function testDoesNotSupportRandomInterfaceIds() public {
        Assert.isFalse(team.supportsInterface(0x01ffc9a8), "");
    }
}
