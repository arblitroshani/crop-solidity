// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "../../contracts/component-CROP/ComponentRole.sol";
import "../../contracts/component-CROP/RoleCreator.sol";
import "./FakeRole.sol";

contract FakeRole1Creator is RoleCreator {
    function createFor(bytes32 spec) external override returns (ComponentRole) {
        return new FakeRole1();
    }
}

contract FakeRole1UpdatedCreator is RoleCreator {
    function createFor(bytes32 spec) external override returns (ComponentRole) {
        return new FakeRole1Updated();
    }
}

contract FakeRole2Creator is RoleCreator {
    function createFor(bytes32 spec) external override returns (ComponentRole) {
        return new FakeRole2();
    }
}
