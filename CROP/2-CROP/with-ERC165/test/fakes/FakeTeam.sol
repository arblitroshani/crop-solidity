// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "../../contracts/component-CROP/Team.sol";
import "../../contracts/component-CROP/ComponentRole.sol";

contract FakeTeam is Team { }
