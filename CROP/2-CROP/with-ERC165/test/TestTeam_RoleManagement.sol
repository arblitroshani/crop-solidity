// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.9.0;

import "truffle/Assert.sol";
import "../contracts/component-CROP/ComponentCore.sol";
import "./fakes/FakeRole.sol";
import "./fakes/FakeRoleCreator.sol";
import "./fakes/FakeTeam.sol";

contract TestTeam_RoleManagement {

    Team internal team;

    bytes32 f1spec = keccak256("FAKEROLE1");
    bytes32 f2spec = keccak256("FAKEROLE2");

    function beforeEach() public {
        team = new FakeTeam();
        team.addRoleCreator(f1spec, new FakeRole1Creator());
        team.addRoleCreator(f2spec, new FakeRole2Creator());
    }

    function testAddRoleOverwritesTheCurrentRoleForTheSameSpec() public {
        // Given 
        address role1Address = team.addRole(f1spec);
        
        // When
        team.addRoleCreator(f1spec, new FakeRole1UpdatedCreator());
        team.addRole(f1spec);

        // Then
        Assert.notEqual(team.getRole(f1spec), role1Address, "");
    }

    function testWeCanRetrieveTheRoleAfterItIsAdded() public {
        // Given 
        address role1Address = team.addRole(f1spec);

        // When
        address receivedRole = team.getRole(f1spec);

        // Then
        Assert.equal(receivedRole, role1Address, "");
    }

    function testGetRoleReturnsEmptyAddressForAnUnknownRole() public {
        // When
        address receivedRole = team.getRole(f2spec);

        // Then
        Assert.isZero(receivedRole, "");
    }

    function testHasRoleReturnsTrueWhenRoleIsAdded() public {
        // Given 
        team.addRole(f1spec);

        // When
        bool hasRole1 = team.hasRole(f1spec);

        // Then
        Assert.isTrue(hasRole1, "");
    }

    function testHasRoleReturnsFalseWhenRoleIsNotAdded() public {
        // When
        bool isPlayingRole2 = team.hasRole(f2spec);

        // Then
        Assert.isFalse(isPlayingRole2, "");
    }

    function testRemoveRoleRendersRoleUnusable() public {
        // Given 
        address role1 = team.addRole(f1spec);

        // When
        FakeRole1 fakeRole1 = FakeRole1(role1);
        Component core = new ComponentCore();
        fakeRole1.setCore(address(core));

        team.removeRole(f1spec);

        // Then
        try fakeRole1.sampleFunction() {
            Assert.fail("");
        } catch Error(string memory /*reason*/) {
            // Failure expected.
        } 
    }

    function testHasRoleReturnsFalseAfterRemoveRole() public {
        // Given
        address role1 = team.addRole(f1spec);
        
        // When
        team.removeRole(f1spec);
        bool hasRole1 = team.hasRole(f1spec);

        // Then
        Assert.isFalse(hasRole1, "");
    }
}
