// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "./ComponentRole.sol";
import "./ComponentCore.sol";
import "./RoleCreator.sol";

abstract contract Team {

    mapping(bytes32 => ComponentRole) internal roles;

    mapping(bytes32 => RoleCreator) internal roleCreators;

    /**
     * @dev Adds a role that should be created from `spec`.
     * 
     * Returns the `address` of the newly created role instance.
     */
    function addRole(bytes32 spec) external returns (address) {
        RoleCreator rc = roleCreators[spec];
        ComponentRole role = rc.createInstance(spec);

        if (role != ComponentRole(address(0))) {
            roles[spec] = role;
            role.setTeam(this);
        }
        return address(role);
    }

    /**
     * @dev Removes role with `spec` from `team`.
     *
     * If `team` does not have the role with `spec`, does nothing.
     */
    function removeRole(bytes32 spec) external {
        if (!this.hasRole(spec)) return; 
        ComponentRole componentRole = roles[spec];
        componentRole.setCore(ComponentCore(address(0)));
        componentRole.setTeam(Team(address(0)));
        delete(roles[spec]);
    }

    /**
     * @dev Returns `true` if `team` has role with `spec`.
     */
    function hasRole(bytes32 spec) external view returns (bool) {
        return address(roles[spec]) != address(0);
    }

    /**
     * @dev Returns the role address that is associated to `spec`. 
     *
     * If `team` hdoes not have the role with `spec`, returns 0x00 address.
     */
    function getRole(bytes32 spec) external returns (address) {
        ComponentRole role = roles[spec];
        if (address(role) != address(0)) {
            role.setTeam(this);
        }
        return address(roles[spec]);
    }

    /**
     * @dev Add a concrete `roleCreator` which is associated to a `spec`. 
     */
    function addRoleCreator(bytes32 spec, RoleCreator roleCreator) external {
        roleCreators[spec] = roleCreator;
    }
}